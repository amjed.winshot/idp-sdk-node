### IDP sdk for NodeJs

Unified typescript SDK library that contains all API calls for IDP.

### Installation

1. install command
```bash
# using yarn
yarn add @gofiled/idp-sdk-node

# using npm
npm install @gofiled/idp-sdk-node
```

### Usage

#### Create a new instance

```javascript

import { IDP } from '@gofiled/idp-sdk-node';

// Create an instance
const idp = new IDP({
      API_URL: 'http://localhost:3000',
      client_id: '<Your_Client_ID>',
      client_secret: '<Your_Client_secret>',
});

```

#### Properties

| Property        | Description                 | Default Value |
|-----------------|-----------------------------|---------------|
| `API_URL`       | `BASE_HTTP_URL` for the IDP | `required`    |
| `client_id`     |                             | `required`    |
| `client_secret` |                             | `required`    |


#### Functions

| Function          | Description                              |
|-------------------|------------------------------------------|
| `introspectToken` | Verify a token & get the user basic info |