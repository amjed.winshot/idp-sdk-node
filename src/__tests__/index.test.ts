import { IDP } from '../index';
describe('Define Monitoring', () => {
  let instance: IDP;
  beforeAll(() => {
    instance = new IDP({
      API_URL: 'http://localhost:3000',
      client_id: 'spotly-api',
      client_secret: 'simpleSecret',
    });
  });
  test('Define  instance', () => {
    expect(instance.tokenService).toBeDefined();
  });
});
