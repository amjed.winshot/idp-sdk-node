/*
 * Developer: Alaa Mokrani
 * Company: Gofield
 * Date: 11/14/2023
 */

import { ClientModelType } from '../client';
import { ScopeModelType } from '../scope';
import { UserModelType } from '../user';

export type AccessTokenModelType = {
  _id: string;
  createdAt: Date;
  accessToken: string;
  accessTokenExpiresAt: Date;
  refreshToken?: string;
  refreshTokenExpiresAt?: Date;
  client: ClientModelType | string;
  user?: UserModelType | string;
  scopes: Array<ScopeModelType | string>;
};
