import { AccessTokenModelType } from './model';
/*
 * Developer: Alaa Mokrani
 * Company: Gofield
 * Date: 11/14/2023
 */

export type AccessTokenCreateType = Pick<
  AccessTokenModelType,
  'accessToken' | 'accessTokenExpiresAt' | 'client' | 'user' | 'scopes' | 'refreshToken' | 'refreshTokenExpiresAt'
>;
