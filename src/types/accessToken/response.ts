/*
 * Developer: Alaa Mokrani
 * Company: Gofield
 * Date: 11/14/2023
 */

export type accessTokenResponseType = {
  name: string;
};
