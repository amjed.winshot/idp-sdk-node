export type IDPConfig = {
  API_URL: string;
  client_id: string;
  client_secret: string;
};
export type GrantIdentifier = 'authorization_code' | 'client_credentials' | 'refresh_token' | 'password' | 'implicit';
export type VerifyTokenResponse =
  | {
      active: false;
      isExpired?: boolean;
    }
  | {
      active: true;
      // issuer of the token (clientId)
      iss: string;
      // cid: ClientId
      cid: string;
      //
      scope: string;
      // sub (subject): Subject of the JWT (the userId)
      sub: string;
      // expiration time
      exp: string;
      // time when the token was issued
      nbf: string;
      // (issued at time): Time at which the JWT was issued; can be used to determine age of the JWT
      iat: string;
      // (JWT ID): Unique identifier; can be used to prevent the JWT from being replayed (allows a token to be used only once)
      jti: string;
      // the client that (user | app) is trying to access
      client_id: string;
      // the user that is trying to access
      username: string;
    };
