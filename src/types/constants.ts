export const ROLES = {
  // Winshot roles
  WINSHOT_MOBILE_USER: 'MOBILE_USER',
  WINSHOT_CRM_USER: 'CRM_USER',
  WINSHOT_SUPERADMIN_USER: 'SUPERADMIN_USER',
  // spotly roles
  SPOTLY_ANIMATOR: 'ANIMATOR',
  SPOTLY_SUPERVISOR: 'SUPERVISOR',
  SPOTLY_AGENCY_USER: 'AGENCY_USER',
  SPOTLY_COMPANY_USER: 'COMPANY_USER',
  SPOTLY_SUPER_ADMIN: 'SUPER_ADMIN',
  //
  IDP_ADMIN: 'IDP_ADMIN',
  IDP_USER: 'IDP_USER',
} as const;

export type RoleType = typeof ROLES[keyof typeof ROLES];

export const CODE_CHALLENGE_METHODS = {
  S256: 'S256',
  plain: 'plain',
} as const;

export type CodeChallengeMethodType = typeof CODE_CHALLENGE_METHODS[keyof typeof CODE_CHALLENGE_METHODS];

export const ALLOWED_GRANTS = {
  client_credentials: 'client_credentials',
  password: 'password',
  refresh_token: 'refresh_token',
  // not implemented yet
  authorization_code: 'authorization_code',
  // not implemented yet
  implicit: 'implicit',
} as const;

export type AllowedGrantType = typeof ALLOWED_GRANTS[keyof typeof ALLOWED_GRANTS];

export const TOKEN_HINTS = {
  access_token: 'access_token',
  refresh_token: 'refresh_token',
  //  TODO not implemented yet
  // id_token: 'id_token',
  //  TODO not implemented yet
  // authorization_code: 'authorization_code',
} as const;

export type TokenHintType = typeof TOKEN_HINTS[keyof typeof TOKEN_HINTS];

export enum STATUS_CODE {
  OK = 200,
  BAD_REQUEST = 400,
  NOT_FOUND = 404,
  INTERNAL_SERVER = 500,
  UNAUTHORIZED = 401,
  DUPLICATED = 409,
  FORBIDDEN = 403,
  GONE = 410,
  EXPIRED = 498,
  LOCKED = 423,
  NOT_ALLOWED = 405,
}
export enum LOGGER_FILES {
  emailBounce = 'loggerBounce',
  signIn = 'loggerSignIn',
  device = 'loggerDevice',
  cheese = 'logger',
  sms = 'loggerSms',
  email = 'loggerEmail',
  socket = 'loggerSocket',
  notification = 'loggerNotification',
}
