/*
 * Developer: Alaa Mokrani
 * Company: Gofield
 * Date: 11/14/2023
 */

export type ScopeModelType = {
  // common
  _id: string;
  createdAt: Date;
  updatedAt: Date;
  //
  name: string;
};
