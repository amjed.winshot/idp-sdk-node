import { GrantIdentifier } from '../common';
import { ScopeModelType } from '../scope';
import { RoleType } from '../constants';
import { UserModelType } from '../user';

export type ClientModelType = {
  //  common
  _id: string;
  createdAt: Date;
  updatedAt: Date;
  isDeleted: boolean;
  // common name
  name: string;
  // this not implemented yet we need it in "code" flow
  redirectUris: string[];
  // this not implemented yet we need it in "code" flow

  allowedGrants: GrantIdentifier[];
  // this is just for a client with client credentials
  secret?: string;
  // TODO
  scopes: Array<string | ScopeModelType>;
  // hwo can access this client based on roles
  roles: Array<RoleType>;
  // if it's true only one session per user is allowed
  oneSessionPerUser: boolean;
  // if it's true the client is trusted by us and can preform actions
  isTrusted: boolean;

  //
  createdBy: string | UserModelType;

  updatedBy: string | UserModelType;
};
