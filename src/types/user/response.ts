import 'reflect-metadata';
import { UserModelType } from './model';
import { RoleType } from '../constants';

export type CommonUserAttributes = Pick<UserModelType, 'username' | 'roles' | 'email' | 'phoneNumber' | 'fullName' | 'login'>;

export type CreateUserType = CommonUserAttributes & {
  language?: string;
  password?: string;
  isVerified?: boolean;
  isAdmin?: boolean;
};
export type UpdateUserType = {
  fullName: string;
  email: string;
  phoneNumber: string;
  //optional for 2auth attributes case n
  login?: string;
  password?: string;
  username?: string;
  roles?: RoleType[] | string[];
  isVerified?: boolean;
  language?: string;
};
export type CreateWithIdType = CommonUserAttributes & {
  _id: string;
  createdAt?: Date;
  updatedAt?: Date;
  isVerified: boolean;
};

export type CreatedUserResponse = Omit<UserModelType, 'password' | 'isDeleted'>;

export type UpdatedUserResponse = CreatedUserResponse & { _id: string };
export type CheckUserResponse = { _id: string };
