import { RoleType } from '../constants';
export type UserModelType = {
  // common
  _id: string;
  createdAt: Date;
  updatedAt: Date;
  //
  username: string;
  login?: string;
  //
  roles: string[];

  fullName?: string;
  isDeleted: boolean;
  isVerified: boolean;
  password?: string;
  // The user logged in based on the `username`
  phoneNumber?: string;
  email?: string;
};
