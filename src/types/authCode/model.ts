/*
 * Developer: Alaa Mokrani
 * Company: Gofield
 * Date: 11/14/2023
 */

import { ClientModelType } from '../client';
import { CodeChallengeMethodType } from '../constants';
import { ScopeModelType } from '../scope';
import { UserModelType } from '../user';

export type AuthCodeModelType = {
  _id: string;
  createdAt: Date;
  updatedAt: Date;
  //
  code: string;
  expiresAt: Date;
  // optional
  user?: string | UserModelType;
  scopes?: Array<string | ScopeModelType>;
  client?: string | ClientModelType;
  codeChallengeMethod?: CodeChallengeMethodType;
  codeChallenge?: string;
  redirectUri?: string;
};
