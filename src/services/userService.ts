import { CommonUserAttributes, CreateWithIdType, CreateUserType, CreatedUserResponse, UpdateUserType, UpdatedUserResponse } from '../types';
import AbstractHttpClient from './abstractHttpClient';

export default class UserService extends AbstractHttpClient {
  create(payload: CreateUserType) {
    return this.instance.post<CreatedUserResponse>('/users', payload);
  }

  update(userId: string, payload: UpdateUserType) {
    return this.instance.put<UpdatedUserResponse>('/users', {
      _id: userId,
      ...payload,
    });
  }

  getById(_id: string) {
    return this.instance.get<CommonUserAttributes & { isVerified: boolean }>(`/users/${_id}`);
  }

  delete(_id: string) {
    return this.instance.delete<never>(`/users/${_id}`);
  }

  setIsVerified(_id: string) {
    return this.instance.post(`/users/${_id}/verify`);
  }

  isDuplicated(
    _id: string,
    payload: {
      username?: string;
      login?: string;
      email?: string;
      phoneNumber?: string;
      manageResponse?: boolean;
    },
  ) {
    if (!_id && !payload) return;
    return this.instance.post<false | void | 'email' | 'phoneNumber' | 'login' | 'username'>(`/users/isDuplicated`, { ...payload, _id });
  }

  /***
   * used for migration
   * @deprecated
   *
   */
  createWithId(secretKey: string, payload: CreateUserType & { _id: string }) {
    return this.instance.post(`/users/withId`, payload);
  }
}
