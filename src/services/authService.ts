import AbstractHttpClient from './abstractHttpClient';
import { IDPConfig } from '../types';

export default class AuthService extends AbstractHttpClient {
  async loginWithCredantials(payload: { client_id: string; client_secret: string } = this.config): Promise<string | undefined> {
    const response = await this.instance.post(`${this.config.API_URL}/oauth2/token`, {
      ...payload,
      grant_type: 'client_credentials',
    });
    if (response?.data?.access_token) return response?.data?.access_token;
    else return undefined;
  }

  /**
   *
   * @param oldPassword : plain text
   * @param newPassword : plain text
   * @param userId
   * @returns
   * compare & change user password
   */
  updatePassword(payload: { userId: string; oldPassword: string; newPassword: string }) {
    return this.instance.put('/auth/password', payload);
  }

  /**
   * @deprecated
   */

  async signIn(payload: {
    password: string;
    username: string;
    client_id: string;
    grant_type: string;
    ip?: string;
    userAgent?: string;
  }): Promise<string | undefined> {
    const currentHeaders = { ...this.instance.defaults.headers };
    if (payload.ip) {
      currentHeaders['x-real-ip'] = payload.ip;
    }

    if (payload.userAgent) {
      currentHeaders['user-agent'] = payload.userAgent;
    }
    const response = await this.instance.post(
      `${this.config.API_URL}/oauth2/token`,
      { password: payload.password, username: payload.username, client_id: payload.client_id, grant_type: payload.grant_type },
      {
        //@ts-ignore
        headers: currentHeaders,
      },
    );
    if (response?.access_token) return response?.access_token;
    else return undefined;
  }
  async requestOtp(payload: { username: string; client_id: string; ip?: string; userAgent?: string }): Promise<void> {
    const currentHeaders = { ...this.instance.defaults.headers };

    if (payload.ip) currentHeaders['x-real-ip'] = payload.ip;
    if (payload.userAgent) currentHeaders['user-agent'] = payload.userAgent;
    await this.instance.post(
      `${this.config.API_URL}/oauth2/requestOtp`,
      { username: payload.username, client_id: payload.client_id, grant_type: 'otp' },
      {
        //@ts-ignore
        headers: currentHeaders,
      },
    );
  }
}
