import AbstractHttpClient from './abstractHttpClient';

export default class CompanyService extends AbstractHttpClient {
  create(companyId: string) {
    return this.instance.post(`/companies/${companyId}`);
  }
}
