import axios, { AxiosInstance } from 'axios';
import { IDPConfig } from '../types';
import AuthService from './authService';

declare module 'axios' {
  export interface AxiosInstance {
    request<T = any>(config: AxiosRequestConfig): Promise<T>;
    get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    head<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
    put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
    patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
  }
}

export default abstract class AbstractHttpClient {
  protected readonly instance: AxiosInstance;
  protected readonly config: IDPConfig;
  private accessToken?: string;

  constructor(config: IDPConfig) {
    this.config = config;
    this.instance = axios.create({
      baseURL: this.config.API_URL,
    });

    this._initializeResponseInterceptor();
  }

  private _initializeResponseInterceptor() {
    this.instance.interceptors.response.use(
      (response) => response.data,
      async (error) => {
        const originalRequest = error.config;

        if (error.response && error.response.status == 401 && error.response?.data?.reason == 'TokenExpired' && !originalRequest._retry) {
          originalRequest._retry = true;
          await this.login();
          originalRequest.headers.Authorization = `Bearer ${this.accessToken}`;
          return this.instance(originalRequest);
        }

        return Promise.reject(error);
      },
    );

    this.instance.interceptors.request.use(
      async (config) => {
        if (!this.accessToken) {
          await this.login();
        }
        config.headers.Authorization = `Bearer ${this.accessToken}`;
        return config;
      },
      null,
      {
        synchronous: false,
      },
    );
  }

  private async login() {
    const response = await axios.post(this.config.API_URL + '/oauth2/token', {
      grant_type: 'client_credentials',
      client_id: this.config.client_id,
      client_secret: this.config.client_secret,
    });
    this.accessToken = response.data.access_token;
  }
}
