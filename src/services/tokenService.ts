import { VerifyTokenResponse } from '../types';
import AbstractHttpClient from './abstractHttpClient';

export default class TokenService extends AbstractHttpClient {
  introspectToken(payload: { token: string; token_type_hint: 'access_token' | 'refresh_token' }) {
    return this.instance.post<VerifyTokenResponse>('/oauth2/introspect', payload);
  }
  issueNewAccessTokenForAUser(payload: { userId: string; client_id: string }) {
    return this.instance.post<unknown>('/accessTokens/issueNewAccessTokenForAUser', payload);
  }
  /**
   *
   * @param password : hashed password
   * @param userId
   * @returns
   * @deprecated
   * update user password
   */
  updatePassword(payload: { userId: string; password: string }) {
    return this.instance.put('/oauth2/password', payload);
  }
}
