import AuthService from './services/authService';
import CompanyService from './services/companyService';
import TokenService from './services/tokenService';
import UserService from './services/userService';
import { IDPConfig } from './types';

export default class IDP {
  private readonly config: IDPConfig;
  tokenService: TokenService;
  userService: UserService;
  companyService: CompanyService;
  authService: AuthService;
  private readonly isNodeServer = typeof process !== 'undefined' && process?.release?.name === 'node';

  constructor(config: IDPConfig) {
    if (!this.isNodeServer) {
      throw new Error('IDP is only supported on Node.js');
    }
    const defaultConfig: Partial<IDPConfig> = {};
    this.config = { ...defaultConfig, ...config };
    this.tokenService = new TokenService(this.config);
    this.userService = new UserService(this.config);
    this.companyService = new CompanyService(this.config);
    this.authService = new AuthService(this.config);
  }
}
